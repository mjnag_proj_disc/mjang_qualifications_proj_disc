# Mike Jang's Qualifications

Mike Jang's qualifications for the Project Discovery Senior Technical Writer position.

- Resume: mjangResume_Project_Discovery.docx
- Writing Samples: listOfWritingSamples.md
- Cover Letter: coverLetter_ProjDiscovery.docx
- Speaking Experience: speakingExperience.md

- Pull request on Nuclei Docs: https://github.com/projectdiscovery/nuclei-docs/pull/120
